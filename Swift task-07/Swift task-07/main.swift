//
//  main.swift
//  Swift task-07
//
//  Created by Владислав Положай on 4/1/22.
//

protocol Names {
    var name : String {get}
    func tellName()
}

class Dad : Names {
    func tellName() {
        print("Father's name is \(name)")
    }
    func buyToy() {
        print("Dad bought a toy")
    }
    var name : String
    var wife : Mom!
    var child1 : Children!
    var child2: Children!
    
    weak var family : Family!
    
    lazy var printFamily : () -> () = {
        [unowned self] in
        family.printFamily()
    }
    lazy var printWife : () -> () = {
        [unowned self] in
        print("Father's wife name is \(self.wife.name)")
    }
    lazy var printChild : () -> () = {
        [unowned self] in
        print("Father's children names are \(self.child1.name) and \(self.child2.name)")
    }
    
    init (name : String) {
        self.name = name
    }
    deinit {
        print("\(name) went for a walk")
    }
}

class Mom : Names {
    func tellName() {
        print("Mothers's name is \(name)")
    }
    func giveCandy() {
        print("Mom gave a candy")
    }
    
    var name: String
    var child1 : Children!
    var child2 : Children!
    
    unowned var husband : Dad
    weak var family : Family!
    
    lazy var printHusband : () -> () = {
        [unowned self] in
        print("Mother's husband name is \(self.husband.name)")
    }
    lazy var printChild : () -> () = {
        [unowned self] in
        print("Mother's children names are \(self.child1.name) and \(self.child2.name)")
    }
    
    init (name : String, husband : Dad) {
        self.name = name
        self.husband = husband
        self.husband.wife = self
    }
    deinit {
        print("\(name) went for a walk")
    }
}

class Children : Names {
    
    func tellName() {
        print("His/her name is \(name)")
    }
    func askForNewToy() {
        print("Dad buy us a new toy pls")
        dad.buyToy()
    }
    func askForToy() -> String {
        return "give me the toy please"
    }
    func askForCandy() {
        print("Mom give us a candy pls")
        mom.giveCandy()
    }
    func askForOtherChildToy() {
        print("\(name) say's to \(otherChild.name): \(otherChild.askForToy())")
    }
    
    var name: String
    unowned var mom: Mom
    unowned var dad : Dad
    weak var otherChild : Children!
    weak var family : Family!
    
    lazy var printDadsName : () -> () = {
        [unowned self] in
        print("\(self.name) said his/her dad's name is \(self.dad.name)")
    }
    lazy var printMomsName : () -> () = {
        [unowned self] in
        print("\(self.name) said his/her mom's name is \(self.mom.name)")
    }
    lazy var printOtherChildsName : () -> () = {
        [unowned self] in
        print("\(self.name)'s bro/sis name is \(self.otherChild.name)")
    }
    init(dad: Dad, mom : Mom, name : String) {
        self.dad = dad
        self.mom = mom
        self.name = name
    }
    deinit{
        print("\(name) went for a walk")
    }
}

class Family {
    func printFamily() {
        print("Family members are \(father.name)\n\(mother.name)\n\(child1.name)\n\(child2.name)")
    }
    let father : Dad!
    let mother : Mom!
    let child1 : Children!
    let child2 : Children!
    
    init(fatherName : String, motherName : String, childName1 : String, childName2 : String) {
        father = Dad(name: fatherName)
        mother = Mom(name: motherName, husband: father)
        child1 = Children(dad: father, mom: mother, name: childName1)
        child2 = Children(dad: father, mom: mother, name: childName2)
        
        self.father.family = self
        self.mother.family = self
        self.child1.family = self
        self.child2.family = self
        self.father.child1 = child1
        self.father.child2 = child2
        self.mother.child1 = child1
        self.mother.child2 = child2
        self.child1.otherChild = child2
        self.child2.otherChild = child1
    }
    deinit {
        print("All family went for a walk")
    }
}

var playGround = true

if playGround{
    let newFamily = Family(fatherName: "Vlad", motherName: "Natalia", childName1: "Pasha", childName2: "Vera")
    
    newFamily.father.printFamily()
    
    print()
    
    newFamily.father.tellName()
    newFamily.father.printWife()
    newFamily.father.printChild()
    
    print()
    
    newFamily.mother.tellName()
    newFamily.mother.printHusband()
    newFamily.mother.printChild()
    
    print()
    
    newFamily.child1.tellName()
    newFamily.child1.printDadsName()
    newFamily.child1.printMomsName()
    newFamily.child1.printOtherChildsName()
    
    print()
    
    newFamily.child2.tellName()
    newFamily.child2.printDadsName()
    newFamily.child2.printMomsName()
    newFamily.child2.printOtherChildsName()
    
    print()
    
    newFamily.child1.askForNewToy()
    newFamily.child1.askForOtherChildToy()
    newFamily.child2.askForOtherChildToy()
    newFamily.child1.askForCandy()
    
    print()
    print("playground ended")
}

